# SONIC THE HEDGEHOG'S MUSIC BOT DOCS

This is the official gitlab documentation repo for sonicthehedgehog.dev's Sonic Music/Soundboard Discord Bots as well as the Five Nights at Freddy's Music/Soundboard Discord Bots.

Last updated: August 24th, 2024.

### Text Files:

- BOTLISTS.txt - Shows botlists our bots are on
- SONGCODES.txt - View the song codes for the Sonic Music/Soundboard bots: Sonic the Hedgehog, Sonic the Werehog, Shadow the Knight
- FNAFSONGCODES.txt - View the song codes for the Five Nights at Freddy's Music/Soundboard bots: Bonnie Bot and Chica Bot
- NINTENDOSONGCODES - View the song codes for the Nintendo Music bot: Cat Peach
- TROUBLESHOOTING.txt - View troubleshooting tips for common issues.

### Websites

[Developer's Website](https://sonicthedev.glitch.me) | [Discord Support Server](https://discord.gg/j9Tt7h2UkG) | [Online Support/Knowledgebase](https://sonicthedev.tawk.help/)

Invite [Sonic the Werehog](https://discord.com/oauth2/authorize?client_id=823697477374050305&permissions=0&scope=bot) | Sonic the Werehog's [Privacy Policy](https://sonicdiscordbot.weebly.com/sonic-privacy.html).

Invite [Bonnie](https://discord.com/oauth2/authorize?client_id=1096332456539455608&permissions=0&scope=bot) | Invite [Chica](https://discord.com/oauth2/authorize?client_id=1100962422468137100&permissions=0&scope=bot) | Bonnie Bot's [Privacy Policy](https://sonicthedev.glitch.me/fnafbotprivacy.html)